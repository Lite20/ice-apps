package net.gamefruit.ice.jotpad;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class EditorView extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    public static boolean mExternalStorageAvailable = false;
    public static boolean mExternalStorageWritable = false;

    public static Context appContext;

    public static EditText text_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_view);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        checkExternalMedia();
        appContext = getApplicationContext();
        if(!(mExternalStorageAvailable)) {
            Toast.makeText(appContext, "No external storage available. ", Toast.LENGTH_LONG).show();

        }

        text_view = (EditText)findViewById(R.id.text_editor);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    private void checkExternalMedia(){
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mExternalStorageAvailable = mExternalStorageWritable = true;

        }

        else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            mExternalStorageAvailable = true;
            mExternalStorageWritable = false;

        }

        else {
            mExternalStorageAvailable = mExternalStorageWritable = false;

        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if(position == 0) {
            try {
                File f = new File(Environment.getExternalStorageDirectory() + "/New Note.txt");
                f.createNewFile();

            }

            catch (IOException e) {
                e.printStackTrace();

            }
        }

        else if(!(mNavigationDrawerFragment.lastPosition == position)) { // Open the new file
            Toast.makeText(EditorView.appContext, "Loading...", Toast.LENGTH_SHORT).show();
            File f = Environment.getExternalStorageDirectory().listFiles()[position - 1];
            try {
                FileInputStream fis = new FileInputStream(f);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader bufferedReader = new BufferedReader(isr);
                EditorView.text_view.setText(null);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    EditorView.text_view.append(line);

                }
            }

            catch (FileNotFoundException e) {
                Toast.makeText(EditorView.appContext, "Failed to find file: " + f.getName(), Toast.LENGTH_LONG).show();
                e.printStackTrace();

            }

            catch (IOException e) {
                Toast.makeText(EditorView.appContext, "Failed to read file: " + f.getName(), Toast.LENGTH_LONG).show();
                e.printStackTrace();

            }
        }

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, PlaceholderFragment.newInstance(position + 1)).commit();

    }

    public void writeToFile(File f, String data){
        try {
            if (!f.exists()) {
                f.createNewFile();

            }

            FileOutputStream writer = new FileOutputStream(f);
            writer.write(data.getBytes());
            writer.flush();
            writer.close();

        }

        catch (FileNotFoundException e) {
            e.printStackTrace();

        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void onSectionAttached(int number) {
        if(number == 1) { // new note

        }

        else { // load note
            mTitle = Environment.getExternalStorageDirectory().listFiles()[number - 2].getName(); // set title to file name

        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(!(actionBar == null)) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(mTitle);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.editor_view, menu);
            restoreActionBar();
            return true;

        }

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;

        }

        return super.onOptionsItemSelected(item);

    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;

        }

        public PlaceholderFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_editor_view, container, false);
            return rootView;

        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((EditorView) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

        }
    }
}
